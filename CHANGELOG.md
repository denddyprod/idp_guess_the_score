# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] - 23-05-2021
### Added
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Create cluster with 1 manager node
  and 2 worker nodes in AWS by using `docker-machine`
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Add `get-docker-secret` pip package
  to enable fetching of docker secrets into the `io` service python code
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Add `portainer` and `portainer-agent`
  services to the stack
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Add an additional worker node in AWS
  to enable high-availability rolling updates
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Add GitLab CD logic to automatically
  deploy changes by passing in the image tag to the Service Webhook provided by `portainer`
- [@denddyprod](https://gitlab.com/denddyprod) Implemented `bussines logic` to validate all predictions
  and give to all users points.

### Changed
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Open required inboundports in the
  AWS `docker-machine` security group (which was automatically created by `docker-machine`)
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Cleanup and refactoring to remove
  comments, name prefixes, unused code etc.
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Add missing secrets to the `io`
  service and update the code to fetch them from the local secret files
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Replace `phpMyAdmin` with `adminer`
  due to port conflicts issues and configure `adminer`
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Change used `mysql` image to enable
  the use of secret files through the `_FILE` variables for sensitive data
- [@denddyprod](https://gitlab.com/denddyprod) Decoupled `auth service` from matches as a service

## [0.5.0] - 17-05-2021
### Added
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Add APIs and serialization schemas
  for the IO service
- [@denddyprod](https://gitlab.com/denddyprod) Implemented `Matches Service` that gets requests from 
  Kong and communicates to `IO service`.
- [@denddyprod](https://gitlab.com/denddyprod) Addded `Basic Authentication` to `Matches Service` with
   username and password protection.
- [@denddyprod](https://gitlab.com/denddyprod) Added & Tested `Access Control Lists` with `Basic Auth` as 
  two groups (gadmins and gusers)
- [@denddyprod](https://gitlab.com/denddyprod) Added, configured and tested `docker-compose.swarm.yml`
- [@denddyprod](https://gitlab.com/denddyprod) Managed sensitive data with `Docker secrets` such as MYSQL_PASS and MYSQL_USER
- [@denddyprod](https://gitlab.com/denddyprod) Changed docker configuration to a `persistent volume` of database

### Changed
- [@denddyprod](https://gitlab.com/denddyprod) Refactored & bug-fixed to `IO Service`
- [@denddyprod](https://gitlab.com/denddyprod) Changed the way of creating database from code to 
  a `init-db.sql` file and added demo datas.
- [@denddyprod](https://gitlab.com/denddyprod) Refactored `Matches service` routes by group access

## [0.4.0] - 09-05-2021
### Added
- [@denddyprod](https://gitlab.com/denddyprod) Test `matches` as a service & route 
  to `Kong API Gateway`
- [@denddyprod](https://gitlab.com/denddyprod) Init and configure all necessary things
  for `Kong API Gateway`
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Flask models for the
  database structure (which will automatically create the corresponding table)
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Network separation of
  the microservices, based on the system architecture
- [@andreicostinlica](https://gitlab.com/andreicostinlica) PyMySQL pip requirement
  for the `io` service to provide driver to connect the flask app to MySQL
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Wait for `mysql`
  service to be fully initialized before starting `io` service, which connects
  to the database, to avoid situations where the database is not ready to accept
  connections from the `io` service

### Changed
- [@denddyprod](https://gitlab.com/denddyprod) Add & refactored networks for 
  `Kong`/`microservices`
- [@denddyprod](https://gitlab.com/denddyprod) Move all `Kong` & `postgress` environment 
  variables from docker-compose.yml to separte .env files
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Change name of `api`
  microservice to `matches` to match system design
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Move environment
  variables from docker-compose.yml to .env file
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Implement best-practice
  flask folder structure and configuration for the `io` service

## [0.3.0] - 08-05-2021
### Added
- [@denddyprod](https://gitlab.com/denddyprod) Write a pseudo-code for bussines logic to
establish a connection between Kong and IO Services

### Changed
- [@denddyprod](https://gitlab.com/denddyprod) Update README to add system
  architecture details

## [0.3.0] - 06-05-2021
### Added
- [@denddyprod](https://gitlab.com/denddyprod) Tested authentication and authorization platform
  of `auth0`
- [@denddyprod](https://gitlab.com/denddyprod) Inited files and all configurations for 
  third-party app, named `auth0`

## [0.2.1] - 25-04-2021
### Added
- [@andrei.orasteanu](https://gitlab.com/andrei.orasteanu) Database structure in
  SQL format
- [@andrei.orasteanu](https://gitlab.com/andrei.orasteanu) Database test dataset
  for testing and development purposes

## [0.2.0] - 19-04-2021
### Added
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Build docker images
  by using GitLab CI
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Initial version
  of docker-compose.yml file, for local development and testing, which deploys
  the three microservices, a MySQL server and a phpMyAdmin service which serves
  as the database management system
### Changed
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Switch to using
  GitLab Registry as the image repository for better integration with GitLab CI

## [0.1.0] - 18-04-2021
### Added
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Dockerfiles for
  the three microservices
- [@andreicostinlica](https://gitlab.com/andreicostinlica) Initialize dummy
  flask application for each microservice
- [@andreicostinlica](https://gitlab.com/andreicostinlica) .dockerignore and
  .gitignore files
- [@andreicostinlica](https://gitlab.com/andreicostinlica) DockerHub account
  and process to build and push the images to the DockerHub repository


## [0.0.1] - 03-04-2021
### Added
- [@denddyprod](https://gitlab.com/denddyprod),
  [@andreicostinlica](https://gitlab.com/andreicostinlica),
  [@andrei.orasteanu](https://gitlab.com/andrei.orasteanu) Initial system design
- [@denddyprod](https://gitlab.com/denddyprod),
  [@andreicostinlica](https://gitlab.com/andreicostinlica),
  [@andrei.orasteanu](https://gitlab.com/andrei.orasteanu) Initial database design
- [@denddyprod](https://gitlab.com/denddyprod) README draft