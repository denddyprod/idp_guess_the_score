from flask import Flask, request
import requests

app = Flask(__name__)


@app.route("/")
def hello():
    return "I am the Auth service!"


URL = "http://io:5000"

@app.route("/users", methods=["GET"])
def getUsers():
    location = '/users'
    response = requests.get(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/users", methods=["POST"])
def addUsers():
    r_data = request.get_json()

    location = '/users'
    response = requests.post(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/users/<id>", methods=["PUT"])
def updateUser():
    r_data = request.get_json()

    location = '/users/' + id
    response = requests.get(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/users/<int:id>", methods=["DELETE"])
def deleteUser(id):

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    location = '/users/' + id
    response = requests.get(URL + location, data=request.data)

    return response.content, response.status_code


if __name__ == "__main__":
    app.run()
