import os
from get_docker_secret import get_docker_secret

class Config(object):
    DB_CONTAINER = os.environ.get("DB_CONTAINER")
    DB_NAME = os.environ.get("MYSQL_DATABASE")
    DB_USER = get_docker_secret("mysql_user")
    DB_PASSWORD = get_docker_secret("mysql_password")
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://%s:%s@%s:3306/%s" % \
                              (DB_USER, DB_PASSWORD, DB_CONTAINER, DB_NAME)
    SQLALCHEMY_TRACK_MODIFICATIONS = False
