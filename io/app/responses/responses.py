def generic_response(status_code, message):
    return message, status_code

def ok(message=''):
    return generic_response(200, message)

def created(message=''):
    return generic_response(201, message)

def bad_request(message=''):
    return generic_response(400, message)

def not_found(message=''):
    return generic_response(404, message)

def conflict(message=''):
    return generic_response(409, message)