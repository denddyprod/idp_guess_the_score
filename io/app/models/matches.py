from app import db


class MatchesModel(db.Model):
    __tablename__ = "matches"
    id        = db.Column(db.Integer, primary_key=True, autoincrement=True)
    team1     = db.Column(db.String(255), nullable=False)
    team2     = db.Column(db.String(255), nullable=False)
    score1    = db.Column(db.Integer)
    score2    = db.Column(db.Integer)
    is_played = db.Column(db.Integer, default=0, nullable=False)
