from app import db


class PointsAllocationsModel(db.Model):
    __tablename__ = "points_allocations"
    id                = db.Column(db.Integer, primary_key=True, autoincrement=True)
    result_type       = db.Column(db.String(20))
    points_for_result = db.Column(db.Integer)
