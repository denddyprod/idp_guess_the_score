from app import db


class UsersModel(db.Model):
    __tablename__ = "users"
    id       = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True, nullable=False)
    score = db.Column(db.Integer, default=0)

    def __init__(self, username, score='0'):
        self.username = username
        self.score = score
    
    def to_json(self):
        return dict(username=self.username, score=self.score)
