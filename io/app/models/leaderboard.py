from app import db


class LeaderboardModel(db.Model):
    __tablename__ = "leaderboard"
    id                = db.Column(db.Integer, primary_key=True)
    user_id           = db.Column(db.Integer, nullable=False)
    aprox_results    = db.Column(db.Integer, default=0)
    correct_results   = db.Column(db.Integer, default=0)
    incorrect_results = db.Column(db.Integer, default=0)
    total_points      = db.Column(db.Integer, default=0)
