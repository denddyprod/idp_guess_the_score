from app import app, db
from flask import request
from app.responses.responses import *
from app.models.predictions import PredictionsModel
from app.schemas.predictions import PredictionsSchema

# Serialization and validation
predictions_schema = PredictionsSchema()

# POST /predictions
@app.route("/predictions", methods=["POST"])
def add_prediction():
    data = request.get_json()

    errors = predictions_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify new object satisfies unique constraint
    prediction = PredictionsModel.query.filter(PredictionsModel.match_id != data['match_id'], \
                                               PredictionsModel.user_id == data['user_id']).first()
    if prediction:
        return conflict()

    # Create new object
    prediction = PredictionsModel(match_id=data['match_id'], \
                         score1=data['score1'], \
                         score2=data['score2'], \
                         user_id=data['user_id'])
    db.session.add(prediction)
    db.session.commit()

    # Return the newly created object
    return created(predictions_schema.jsonify(prediction))


# GET /predictions
@app.route("/predictions", methods=["GET"])
def get_predictions():
    # Get all objects
    predictions = PredictionsModel.query.all()

    # Return all objects
    return ok(predictions_schema.jsonify(predictions, many=True))


# PUT /predictions/:id
@app.route("/predictions/<int:id>", methods=["PUT"])
def update_prediction(id):
    data = request.get_json()
    errors = predictions_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify object to be updated exists
    prediction = PredictionsModel.query.get(id)
    if not prediction:
        return not_found()

    # Update object
    prediction.match_id = data['match_id']
    prediction.score1 = data['score1']
    prediction.score2 = data['score2']
    prediction.user_id = data['user_id']

    db.session.commit()

    return ok()


# DELETE /predictions/:id
@app.route("/predictions/<int:id>", methods=["DELETE"])
def delete_prediction(id):
    # Get object
    prediction = PredictionsModel.query.get(id)

    # Verify object exists
    if not prediction:
        return not_found()
    
    # Delete object
    db.session.delete(prediction)
    db.session.commit()

    return ok()
