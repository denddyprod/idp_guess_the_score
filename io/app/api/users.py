from app import app, db
from app.models.users import UsersModel
from flask import request, jsonify
from app.responses.responses import *
from app.schemas.users import UsersSchema


# Serialization and validation
# Serialize data returned via the /api/cities GET API
# Validate the body for the PUT API
user_schema = UsersSchema()
# Serialize the id of the newly created object via the POST API
user_schema_id_only = UsersSchema(only=["id"])
# Validate the body for the POST API
user_schema_id_exclude = UsersSchema(exclude=["id"])

# POST /users
@app.route("/users", methods=["POST"])
def create():
    data = request.get_json()
    username = data["username"]
    score = data["score"]

    errors = user_schema_id_exclude.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()

    user = UsersModel(username=username, score=score)

    db.session.add(user)
    db.session.commit()

    return created(user.to_json())

# GET /users
@app.route("/users", methods=["GET"])
def getAll():
    # Get all objects
    users = UsersModel.query.all()
    return ok(user_schema.jsonify(users, many=True))

# GET /users/id
@app.route("/users/<id>", methods=["GET"])
def getUser(id):
    # Get all objects
    user = UsersModel.query.get(id)
    return ok(user_schema.jsonify(user))

# PUT /users/:id
@app.route("/users/<int:id>", methods=["PUT"])
def update_user(id):
    data = request.get_json()
    username = data["username"]
    score = data["score"]

    errors = user_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify object to be updated exists
    user = UsersModel.query.get(id)
    if not user:
        return not_found()

    # Update object
    user.score = data['score']
    user.username = data['username']

    db.session.commit()

    return ok()

# DELETE /users/:id
@app.route("/users/<int:id>", methods=["DELETE"])
def delete_user(id):
    # Get object
    user = UsersModel.query.get(id)

    # Verify object exists
    if not user:
        return not_found()

    # Delete object
    db.session.delete(user)
    db.session.commit()

    return ok()
