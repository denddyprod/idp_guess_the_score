from app import app, db
from flask import request
from app.responses.responses import *
from app.models.matches import MatchesModel
from app.schemas.matches import MatchesSchema

# Serialization and validation
matches_schema = MatchesSchema()

# POST /matches
@app.route("/matches", methods=["POST"])
def add_match():
    data = request.get_json()
    errors = matches_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()

    # Create new object
    match = MatchesModel(team1=data['team1'], \
                         team2=data['team2'], \
                         score1=data['score1'], \
                         score2=data['score2'], \
                         is_played=data['is_played'])
    db.session.add(match)
    db.session.commit()

    # Return the newly created object
    return created(matches_schema.jsonify(match))


# GET /matches
@app.route("/matches", methods=["GET"])
def get_matches():
    # Get all objects
    matches = MatchesModel.query.all()

    # Return all objects
    return ok(matches_schema.jsonify(matches, many=True))


# PUT /matches/:id
@app.route("/matches/<int:id>", methods=["PUT"])
def update_match(id):
    data = request.get_json()
    
    errors = matches_schema.validate(data)
    # Validate body
    if errors or not data:
        return bad_request()
    
    # Verify object to be updated exists
    match = MatchesModel.query.get(id)
    if not match:
        return not_found()
    
    # Update object
    match.team1 = data['team1']
    match.team2 = data['team2']
    match.score1 = data['score1']
    match.score2 = data['score2']
    match.is_played = data['is_played']

    db.session.commit()

    return ok()


# DELETE /matches/:id
@app.route("/matches/<int:id>", methods=["DELETE"])
def delete_match(id):
    # Get object
    match = MatchesModel.query.get(id)

    # Verify object exists
    if not match:
        return not_found()
    
    # Delete object
    db.session.delete(match)
    db.session.commit()

    return ok()
