from app import mm
from marshmallow import fields
from marshmallow.validate import Length

class UsersSchema(mm.SQLAlchemyAutoSchema):

    id = fields.Integer(primary_key=True)
    username = fields.String(required=True, validate=Length(min=4), attribute="username")
    score = fields.Integer(required=True, default=0)