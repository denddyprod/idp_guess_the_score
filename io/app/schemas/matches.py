from app import mm
from marshmallow import fields
from marshmallow.validate import Length


class MatchesSchema(mm.SQLAlchemyAutoSchema):
    id        = fields.Integer(primary_key=True)
    team1     = fields.String(required=True, validate=Length(max=255))
    team2     = fields.String(required=True, validate=Length(max=255))
    score1    = fields.Integer()
    score2    = fields.Integer()
    is_played = fields.Integer(required=True)
