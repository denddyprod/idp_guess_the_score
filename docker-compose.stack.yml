version: "3.8"

services:
    auth:
        image: registry.gitlab.com/denddyprod/idp_guess_the_score/idp-auth:40
        ports:
            - 6000:6000
        environment:
            FLASK_APP: wsgi.py
            FLASK_ENV: production
        deploy:
            replicas: 2
            placement:
                max_replicas_per_node: 1
                constraints: [node.role != manager]
            update_config:
                order: start-first
            rollback_config:
                order: stop-first
        networks:
            - backend-to-backend

    io:
        image: registry.gitlab.com/denddyprod/idp_guess_the_score/idp-io:40
        ports:
            - 3000:5000
        environment:
            DB_CONTAINER: mysql
            FLASK_APP: wsgi.py
            FLASK_ENV: production
            MYSQL_DATABASE: idp
            MYSQL_PASSWORD: /run/secrets/mysql_password
            MYSQL_USER: /run/secrets/mysql_user
        deploy:
            replicas: 2
            placement:
                max_replicas_per_node: 1
                constraints: [node.role != manager]
            update_config:
                order: start-first
            rollback_config:
                order: stop-first        
        secrets:
            - mysql_password
            - mysql_user
        networks:
            - backend-to-backend
            - database

    matches:
        image: registry.gitlab.com/denddyprod/idp_guess_the_score/idp-matches:40
        ports:
            - 5000:5000
        environment:
            FLASK_APP: wsgi.py
            FLASK_ENV: production
        deploy:
            replicas: 2
            placement:
                max_replicas_per_node: 1
                constraints: [node.role != manager]
            update_config:
                order: start-first
            rollback_config:
                order: stop-first
        networks:
            - backend-to-backend

    mysql:
        image: mysql:5.7
        environment:
            MYSQL_DATABASE: idp
            MYSQL_PASSWORD_FILE: /run/secrets/mysql_password
            MYSQL_ROOT_PASSWORD_FILE: /run/secrets/mysql_root_password
            MYSQL_USER_FILE: /run/secrets/mysql_user
        deploy:
            placement:
                constraints: [node.role == manager]
        volumes:
            - ./database/init-db.sql:/docker-entrypoint-initdb.d/init-db.sql
            - db-volume:/var/lib/mysql
        secrets:
            - mysql_password
            - mysql_root_password
            - mysql_user
        networks:
            - database
            - database-admin

    adminer:
        image: adminer:latest
        ports:
            - 8080:8080
        networks:
            - database-admin
            - frontend-to-backend
        environment:
            ADMINER_DEFAULT_SERVER: mysql
        deploy:
            placement:
                constraints: [node.role == manager]

    kong:
        image: kong:latest
        volumes:
            - ./kong:/usr/local/kong/declarative
        environment:
            KONG_DATABASE: "off"
            KONG_DECLARATIVE_CONFIG: /usr/local/kong/declarative/kong-swarm.yml
            KONG_PROXY_ACCESS_LOG: /dev/stdout
            KONG_ADMIN_ACCESS_LOG: /dev/stdout
            KONG_PROXY_ERROR_LOG: /dev/stderr
            KONG_ADMIN_ERROR_LOG: /dev/stderr
            KONG_ADMIN_LISTEN: 0.0.0.0:8001, 0.0.0.0:8444 ssl
        ports:
            - 8000:8000
            - 8001:8001
        deploy:
            placement:
                constraints: [node.role == manager]
        networks:
            - backend-to-backend
            - frontend-to-backend
    portainer-agent:
        image: portainer/agent
        volumes:
            - /var/run/docker.sock:/var/run/docker.sock
            - /var/lib/docker/volumes:/var/lib/docker/volumes
        networks:
            - agent_network
        deploy:
            mode: global
            placement:
                constraints: [node.platform.os == linux]
        
    portainer:
        image: portainer/portainer-ce
        command: -H tcp://tasks.portainer-agent:9001 --tlsskipverify
        ports:
            - "9000:9000"
            - "8002:8000"
        volumes:
            - portainer_data:/data
        networks:
            - agent_network
            - frontend-to-backend
        deploy:
            mode: replicated
            replicas: 1
            placement:
                constraints: [node.role == manager]

volumes:
    db-volume:
    portainer_data:

networks:
    agent_network:
    backend-to-backend:
    database:
    database-admin:
    frontend-to-backend:

secrets:
    mysql_password:
        external: true
    mysql_root_password:
        external: true
    mysql_user:
        external: true
