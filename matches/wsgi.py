from flask import Flask, request
import requests

app = Flask(__name__)


@app.route("/")
def hello():
    return "I am the Matches service!"


URL = "http://io:5000"


@app.route("/validate", methods=["POST"])
def validate():

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    location = '/predictions'
    response = requests.get(URL + location, data=request.data)
    predictions = response.json()
    
    location = '/points'
    response = requests.get(URL + location, data=request.data)
    points = response.json()

    location = '/matches'
    response = requests.get(URL + location, data=request.data)
    matches = response.json()

    for pred in predictions:
        for match in matches:
            if match['is_played'] == 1 and pred['match_id'] == match['id']:
                
                # wrong prediction
                if pred['score1'] != match['score1'] and pred['score2'] != match['score2']:
                    
                    for point in points:
                        if point['result_type'] == "incorrect_result":
                            # Update user leaderboard
                            location = '/leaderboard/'+str(pred['user_id'])
                            response = requests.get(URL + location, data=request.data)
                            userLeaderboard = response.json()
                            userLeaderboard['incorrect_results'] = userLeaderboard['incorrect_results'] + 1

                            location = '/leaderboard/'+str(userLeaderboard['id'])
                            response = requests.put(URL + location, json=userLeaderboard, headers={"Content-Type": "application/json"})
                            
                            # Delete prediction
                            location = '/predictions/'+str(pred['id'])
                            response = requests.delete(URL + location)

                            continue

                # correct result
                if pred['score1'] == match['score1'] and pred['score2'] == match['score2']:
                    # equal result
                    if pred['score1'] == pred['score2']:
                        for point in points:
                            if point['result_type'] == "equal_result":
                                # Update user leaderboard
                                location = '/leaderboard/'+str(pred['user_id'])
                                response = requests.get(URL + location, data=request.data)
                                userLeaderboard = response.json()
                                userLeaderboard['correct_results'] = userLeaderboard['correct_results'] + 1
                                userLeaderboard['total_points'] = userLeaderboard['total_points'] + 2

                                location = '/leaderboard/'+str(userLeaderboard['id'])
                                response = requests.put(URL + location, json=userLeaderboard, headers={"Content-Type": "application/json"})   

                                # Update user score
                                location = '/users/'+ str(pred['user_id'])
                                response = requests.get(URL + location, data=request.data)
                                user = response.json()
                                user['score'] = user['score'] + 2

                                location = '/users/'+str(user['id'])
                                response = requests.put(URL + location, json=user, headers={"Content-Type": "application/json"})
                                
                                # Delete prediction
                                location = '/predictions/'+str(pred['id'])
                                response = requests.delete(URL + location)

                                continue
                    else:
                        # correct result
                        for point in points:
                            if point['result_type'] == "correct_result":
                                # Update user leaderboard
                                location = '/leaderboard/'+str(pred['user_id'])
                                response = requests.get(URL + location, data=request.data)
                                userLeaderboard = response.json()
                                userLeaderboard['correct_results'] = userLeaderboard['correct_results'] + 1
                                userLeaderboard['total_points'] = userLeaderboard['total_points'] + 3

                                location = '/leaderboard/'+str(userLeaderboard['id'])
                                response = requests.put(URL + location, json=userLeaderboard, headers={"Content-Type": "application/json"})
                        
                                # Update user
                                location = '/users/'+ str(pred['user_id'])
                                response = requests.get(URL + location, data=request.data)
                                user = response.json()
                                user['score'] = user['score'] + 3

                                location = '/users/'+str(str(user['id']))
                                response = requests.put(URL + location, json=user, headers={"Content-Type": "application/json"})
                                
                                # Delete prediction
                                location = '/predictions/'+str(pred['id'])
                                response = requests.delete(URL + location)

                                continue
                
                print(pred['score1'], match['score1'], match['score2'], pred['score2'], 'aici')
                # partially correct result
                if (pred['score1'] != match['score1'] and pred['score2'] == match['score2']) or (pred['score1'] ==
                 match['score1'] and pred['score2'] != match['score2']):
                    
                    for point in points:
                        if point['result_type'] == "aprox_result":
                            # Update user leaderboard
                            location = '/leaderboard/'+str(pred['user_id'])
                            response = requests.get(URL + location, data=request.data)
                            userLeaderboard = response.json()
                            userLeaderboard['aprox_results'] = userLeaderboard['aprox_results'] + 1
                            userLeaderboard['total_points'] = userLeaderboard['total_points'] + 1

                            location = '/leaderboard/'+str(userLeaderboard['id'])
                            response = requests.put(URL + location, json=userLeaderboard, headers={"Content-Type": "application/json"})   
                            
                            # Update user
                            location = '/users/'+ str(pred['user_id'])
                            response = requests.get(URL + location, data=request.data)
                            user = response.json()
                            user['score'] = user['score'] + 1

                            location = '/users/'+str(user['id'])
                            response = requests.put(URL + location, json=user, headers={"Content-Type": "application/json"})
                            
                            # Delete prediction
                            location = '/predictions/'+str(pred['id'])
                            response = requests.delete(URL + location)

                            continue

    return "all prediction validated", 200

@app.route("/predictions", methods=["GET"])
def getPredictions():
    location = '/predictions'
    response = requests.get(URL + location, data=request.data)
    return response.content, response.status_code

@app.route("/predictions", methods=["POST"])
def addPrediction():
    r_data = request.get_json()

    location = '/predictions'
    response = requests.post(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/predictions/<id>", methods=["PUT"])
def updatePrediction(id):
    r_data = request.get_json()

    location = '/predictions/' + id
    response = requests.put(URL + location,  json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/predictions/<id>", methods=["DELETE"])
def deletePrediction(id):
    location = '/predictions/' + id
    response = requests.delete(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/leaderboard", methods=["GET"])
def getLeaderboard():
    location = '/leaderboard'
    response = requests.get(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/leaderboard", methods=["POST"])
def addLeaderboard():
    r_data = request.get_json()

    location = '/leaderboard'
    response = requests.post(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/leaderboard/<id>", methods=["PUT"])
def updateLeaderboard(id):
    r_data = request.get_json()

    location = '/leaderboard/' + id
    response = requests.put(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/leaderboard/<id>", methods=["DELETE"])
def deleteLeaderboard():
    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    location = '/leaderboard/' + id 
    response = requests.delete(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/matches", methods=["GET"])
def viewMatches():
    location = '/matches'
    response = requests.get(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/matches", methods=["POST"])
def addmatch():

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    r_data = request.get_json()

    location = '/matches'
    response = requests.post(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/matches/<id>", methods=["PUT"])
def updateMatch(id):

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    r_data = request.get_json()

    location = '/matches/' + id
    response = requests.put(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/matches/<id>", methods=["DELETE"])
def deleteMatch(id):

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    location = '/matches/' + id
    response = requests.delete(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/points", methods=["GET"])
def getPoints():
    location = '/points'
    response = requests.get(URL + location, data=request.data)

    return response.content, response.status_code


@app.route("/points", methods=["POST"])
def addPoint():

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    r_data = request.get_json()

    location = '/points'
    response = requests.post(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/points/<id>", methods=["PUT"])
def updatePoint(id):

    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401
    
    r_data = request.get_json()

    location = '/points/' + id
    response = requests.put(URL + location, json=r_data, headers=request.headers)

    return response.content, response.status_code


@app.route("/points/<id>", methods=["DELETE"])
def deletePoint(id):
    userGroup = request.headers["X-Consumer-Groups"]
    
    if userGroup != 'gadmin':
        return 'unathorized', 401

    location = '/points/' + id
    response = requests.get(URL + location, data=request.data)

    return response.content, response.status_code

if __name__ == "__main__":
    app.run()
