## IDP | Guess The Score v.1.0.0

![Logo World Cup Guess The Score](img/logo.jpeg)

### Descriere
“Guess my Score” este noul mod de a trăi fotbalul. Ne propunem să implementăm o
aplicație dedicată fotbalului pentru a permite fiecărui să provoce prietenii în jocul de
predicție al unui scor. Aplicația va permite participanților de a ghici scorurile din timpul
partidelor de fotbal. Drept logică va include un
sistem de monitorizare a punctelor obținute în
dependeță de predicția sa. Pentru un rezultat exact
implică primirea a 3 puncte - un rezultat corect, dar
inexact, implică primirea a 2 puncte - remiza implică
primirea a 1 punct - un rezultat greșit implică 0
puncte

### Arhitectura
Aplicația „Guess The Score” va rula pe un cluster format din 1 manager și 2 workers,
incluzând componente de orchestrare. Se vor folosi secrete, din punct de vedere a securității
datelor sensibile. Microserviciile din aplicație vor fi separate logic prin intermediul rețelelor.

![Arhitectura](img/arhitectura.png)

### Kong API Gateway
Am ales Kong pentru că este unul dintre liderii din domeniul Cloud în materie de soluții
de API Gateway. Kong este o componentă aflată între clienți și o colecție de servicii backend,
astfel decuplând interfața pentru clienți de implementarea backendului. Acest instrument
este bazat pe NGinx, iar utilizarea acestuia se face printr-o simplă furnizare de configurație
YML. Drept scop va avea de a intercepta traficul venit, în funcție de reguli specificate de
utilizator și apoi îl redirectează către alte servicii.

![Kong API Gateway](img/kong.png)

### Alte detalii
`Baza de date: MySQL`
`Limbaj: Python`

### Echipa:
* Denis BELÎI – 341C5
* Andrei-Costin LICĂ – 342C5
* Andrei ORĂȘTEANU – 343C3
* Dragoș RADU – 342C3

