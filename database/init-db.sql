﻿-- =============================================================================
-- Diagram Name: db
-- Created on: 5/21/2021 12:46:30 PM
-- Diagram Version: 
-- =============================================================================


CREATE DATABASE IF NOT EXISTS `idp`;

USE `idp`;

SET FOREIGN_KEY_CHECKS=0;

-- Drop table points_allocations
DROP TABLE IF EXISTS `points_allocations`;

CREATE TABLE `points_allocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `result_type` varchar(50) NOT NULL,
  `points_for_result` int(11) NOT NULL,
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

INSERT INTO `points_allocations` (id, result_type, points_for_result)
VALUES (1, 'correct_result', 3);
INSERT INTO `points_allocations` (id, result_type, points_for_result)
VALUES (2, 'aprox_result', 2);
INSERT INTO `points_allocations` (id, result_type, points_for_result)
VALUES (3, 'equal_result', 1);
INSERT INTO `points_allocations` (id, result_type, points_for_result)
VALUES (4, 'incorrect_result', 0);

-- Drop table matches
DROP TABLE IF EXISTS `matches`;

CREATE TABLE `matches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `team1` varchar(255) NOT NULL,
  `team2` varchar(255) NOT NULL,
  `score1` int(11),
  `score2` int(11),
  `is_played` int(11) DEFAULT '0',
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

INSERT INTO `matches` (id, team1, team2, score1, score2, is_played)
VALUES (1, 'FC Barcelona', 'FC Real Madrid', 1, 2, 1);
INSERT INTO `matches` (id, team1, team2, score1, score2, is_played)
VALUES (2, 'FC Manchester United', 'FC Chelsea', 2, 2, 1);
INSERT INTO `matches` (id, team1, team2, score1, score2, is_played)
VALUES (3, 'FC Fullham', 'FC Villarreal', 0, 2, 1);
INSERT INTO `matches` (id, team1, team2, score1, score2)
VALUES (4, 'FC Wolves', 'FC Liverpool', 0, 0);

-- Drop table users
DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `score` int(11) DEFAULT '0',
  PRIMARY KEY(`id`)
)
ENGINE=INNODB;

INSERT INTO `users` (id, username, score)
VALUES (1, 'admin', 0);
INSERT INTO `users` (id, username, score)
VALUES (2, 'denis', 0);
INSERT INTO `users` (id, username, score)
VALUES (3, 'andrei', 0);

-- Drop table leaderboard
DROP TABLE IF EXISTS `leaderboard`;

CREATE TABLE `leaderboard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `aprox_results` int(11) DEFAULT '0',
  `correct_results` int(11) DEFAULT '0',
  `incorrect_results` int(11) DEFAULT '0',
  `total_points` int(11) DEFAULT '0',
  PRIMARY KEY(`id`),
  CONSTRAINT `Ref_01` FOREIGN KEY (`user_id`)
    REFERENCES `users`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE=INNODB;

INSERT INTO `leaderboard` (id, user_id)
VALUES (1, 1);
INSERT INTO `leaderboard` (id, user_id)
VALUES (2, 2);
INSERT INTO `leaderboard` (id, user_id)
VALUES (3, 3);

-- Drop table leaderboard
DROP TABLE IF EXISTS `predictions`;

CREATE TABLE `predictions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `match_id` int(11) NOT NULL,
  `score1` int(11) NOT NULL,
  `score2` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY(`id`),
  CONSTRAINT `Ref_02` FOREIGN KEY (`user_id`)
    REFERENCES `users`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `Ref_03` FOREIGN KEY (`match_id`)
    REFERENCES `matches`(`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)
ENGINE=INNODB;

INSERT INTO `predictions` (id, match_id, score1, score2, user_id)
VALUES (1, 1, 1, 2, 2);
INSERT INTO `predictions` (id, match_id, score1, score2, user_id)
VALUES (2, 1, 3, 3, 3);
INSERT INTO `predictions` (id, match_id, score1, score2, user_id)
VALUES (3, 2, 2, 2, 1);
INSERT INTO `predictions` (id, match_id, score1, score2, user_id)
VALUES (4, 3, 0, 1, 3);
INSERT INTO `predictions` (id, match_id, score1, score2, user_id)
VALUES (5, 4, 0, 0, 3);

SET FOREIGN_KEY_CHECKS=1;
